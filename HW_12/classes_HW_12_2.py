from abc import ABC, abstractmethod


class Point(object):
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y


class Figure(ABC):
    name = 'Figure'

    @abstractmethod
    def area(self):
        print("Calculate the area of the figure")

    @abstractmethod
    def length(self):
        print("Calculate the perimeter of the figure or the length of the circle")

    def __str__(self):
        return f'{self.name}, area = {self.area()}, perimeter(length) = {self.length()}'


class Circle(Figure):
    name = 'Circle'

    def __init__(self, center: Point, radius):
        self.radius = radius
        self.center = center

    def area(self):
        return round(3.14 * self.radius ** 2, 2)

    def length(self):
        return round(2 * 3.14 * self.radius, 2)


class Triangle(Figure):
    name = 'Triangle'

    def __init__(self, point1: Point, point2: Point, point3: Point):
        self.__point1 = point1
        self.__point2 = point2
        self.__point3 = point3
        self.length_of_side()

    def length_of_side(self):
        self.side1 = ((self.__point2.x - self.__point1.x) ** 2 + (self.__point2.y - self.__point1.y) ** 2) ** 0.5
        self.side2 = ((self.__point3.x - self.__point2.x) ** 2 + (self.__point3.y - self.__point2.y) ** 2) ** 0.5
        self.side3 = ((self.__point3.x - self.__point1.x) ** 2 + (self.__point3.y - self.__point1.y) ** 2) ** 0.5
        if (self.side1 + self.side2) <= self.side3 or (self.side2 + self.side3) <= self.side1 or (
                self.side1 + self.side3) <= self.side2:
            self.half_perimeter = -1
        else:
            self.half_perimeter = (self.side1 + self.side2 + self.side3) / 2

    def area(self):
        if self.half_perimeter == -1:
            return -1
        else:
            return round(
                (self.half_perimeter * (self.half_perimeter - self.side1) * (self.half_perimeter - self.side2) * (
                        self.half_perimeter - self.side3)) ** 0.5)

    def length(self):
        if self.half_perimeter == -1:
            return -1
        else:
            return round(self.side1 + self.side2 + self.side3)


class Square(Figure):
    name = 'Square'

    def __init__(self, point1: Point, point2: Point):
        self.__point1 = point1
        self.__point2 = point2
        self.length_of_side()

    def length_of_side(self):
        self.side = ((self.__point2.x - self.__point1.x) ** 2 + (self.__point2.y - self.__point1.y) ** 2) ** 0.5

    def area(self):
        return round(self.side ** 2)

    def length(self):
        return round(self.side * 4)
