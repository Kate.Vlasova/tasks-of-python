from classes_HW_12_2 import Point, Figure, Triangle, Circle, Square

list_of_figures = []
list_of_figures.append(Triangle(Point(2, 3), Point(2, 3), Point(7, 9)))
list_of_figures.append(Square(Point(5, 8), Point(10, 12)))
list_of_figures.append(Circle(Point(5, 8), 12))
list_of_areas = []
for figure in list_of_figures:
    list_of_areas.append(figure.area())

print(*[f"{list_of_figures[i].name}'s area = {list_of_areas[i]}" for i in range(len(list_of_areas))])
print(*[f"{i.name}'s area = {i.area()}" for i in list_of_figures])
