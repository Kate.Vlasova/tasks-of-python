def invalid_value(value):
    try:
        float(value)
    except ValueError:
        return True
    else:
        return False


def division_error(value):
    try:
        1 / value
    except ZeroDivisionError:
        return True
    else:
        return False
