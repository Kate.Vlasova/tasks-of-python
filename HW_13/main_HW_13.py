from func_HW_13 import Calculator
from exceptions_HW_13 import invalid_value, division_error


def main():
    a = input("Enter number a: \n")
    b = input("Enter number b: \n")
    calc = input("Enter '+', '-', '*', '/': \n")
    return Calculator(a, b, calc).result()


while True:
    print(main())
