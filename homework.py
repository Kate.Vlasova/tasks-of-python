import copy
import math
import random
import datetime


# Task 1.1
def operations(a, b):
    return a + b, a - b, a * b


# Task 1.2
def counter(x, y):
    return (abs(x) - abs(y)) / (1 + abs(x * y))


# Task 1.3
def cube(l):
    return l ** 3, l * l * 6


# Task 1.4
def mean(a, b):
    return (a + b) / 2


def geometric_mean(a, b):
    return (a * b) ** 0.5


# Task 1.5
def hypotenuse(a, b):
    return (a * a + b * b) ** 0.5


def triangle_areas(a, b):
    return 0.5 * a * b


# Task 2.1
def symbol_3(string):
    return string[2]


# Task 2.2
def sec_symbol_from_end(string):
    return string[-2]


# Task 2.3
def first_five(string):
    return string[0:5]


# Task 2.4
def without_two_last_symbols(string):
    return string[:-2]


# Task 2.5
def even_symbols(string):
    return string[::2]


# Task 3.1
def millennium(number):
    if number % 1000 == 0:
        return 'millennium'
    return False


# Task 3.2
def wedding(people_amount):
    if people_amount > 50:
        return "Ресторан"
    elif people_amount > 20:
        return "Кафе"
    elif people_amount > 0:
        return "Дом"
    elif people_amount == 0:
        return "Свадьба отменяется"
    return False


# Task 3.3
def str_len(string):
    if len(string) > 10:
        return string + "!!!"
    else:
        return string[1]


# Task 3.4
def print_center_letter(string):
    string = str(string)
    if string[math.ceil(len(string) / 2) - 1] == string[0]:
        return string[math.ceil(len(string) / 2) - 1], string[1:-1]
    else:
        return string[math.ceil(len(string) / 2) - 1]


# Task 4.1
def mult_neg_2(some_list):
    new_list = []
    for i in some_list:
        new_list.append(i * (-2))
    return new_list


# Task 4.2
def even_list(some_list):
    even_numbers = []
    for i in some_list:
        if i % 2 == 0:
            even_numbers.append(i)
    return even_numbers


# Task 4.3
def dict_edit(dictionary):
    new_dict = {}
    for i in dictionary.keys():
        new_dict[i + str(len(i))] = dictionary.get(i)
    return new_dict


# Task 4.4
def shift_list(some_list):
    new_list = some_list[1:]
    new_list.append(some_list[0])
    return new_list


# Task 4.5
def fibonacci_list():
    list_of_fibonacci = [1, 1]
    for i in range(1, 14):
        list_of_fibonacci.append(list_of_fibonacci[i] + list_of_fibonacci[i - 1])
    # ниже решение циклом while
    # i = 1
    # list_of_fibonacci = [1, 1]
    # while i < 14:
    #     list_of_fibonacci.append(list_of_fibonacci[i] + list_of_fibonacci[i - 1])
    #     i+=1
    return list_of_fibonacci


# Task 5.1
def simple_calc(x, y, operator):
    x = float(x)
    y = float(y)
    if operator == "+":
        return x + y
    elif operator == "-":
        return x - y
    elif operator == "*":
        return x * y
    elif operator == "/":
        if y == 0:
            return "ZeroDivisionError"
        return x / y
    else:
        return "ErrorOperator"


# Task 5.2
def sum_elements_of_numbers(nmb):
    nmb = str(nmb)
    summ = 0
    multiply = 1
    for i in nmb:
        summ += int(i)
        multiply *= int(i)
    return summ, multiply


# Task 5.3
def friendly_checker():
    list_friendly_numbers = []
    for i in range(200, 301):
        sum_divider = sum(j for j in range(1, i) if i % j == 0)
        if i == sum(j for j in range(1, sum_divider) if sum_divider % j == 0):
            list_friendly_numbers.append((i, sum_divider))
    return list_friendly_numbers


# Task 5.4
def get_sum(num):
    summ = 0
    num = int(num)
    for i in range(1, num + 1):
        summ += 1 / i
    return summ


# Task 5.5
def changed_by_max_value(array):
    for index, value in enumerate(array):
        if value % 2 == 0:
            array[index] = max(array)
    return array


# Task 5.6
def incr(sp):
    counter = 0
    increase_in_past = False
    for i in range(len(sp[:-1])):
        if sp[i + 1] > sp[i] and not increase_in_past:
            counter += 1
        increase_in_past = sp[i + 1] > sp[i]
    return counter


# Task 5.7
def change_number(matrix):
    for i in range(len(matrix)):
        index_max_value = matrix[i].index(max(matrix[i]))
        swap = matrix[i][i]
        matrix[i][i] = matrix[i][index_max_value]
        matrix[i][index_max_value] = swap
    return matrix


# Task 5.8
def str_reverse(stroka):
    list = stroka.split(" ")
    list.reverse()
    new_stroka = ' '.join(list)
    return new_stroka


# Task 5.9
def dividers(m, n):
    dict_of_dilivers = {}
    for i in range(m, n + 1):
        dict_of_dilivers[i] = ([j for j in range(2, i) if i % j == 0])
    return dict_of_dilivers


# Task 5.10
def info_trains(trains):
    dict_of_longway_trains = {}
    for i in trains:
        time_arrival = trains.get(i)[0][1]
        time_departure = trains.get(i)[1][1]
        if time_arrival < time_departure:
            return 'Incorrect data!'
        if time_departure + datetime.timedelta(hours=7, minutes=20) < time_arrival:
            dict_of_longway_trains[i] = trains.get(i)
    return dict_of_longway_trains


# Task 6.1
def create_matrix(start_number, end_number, num_rows, num_columns):
    # with the list generator
    return [[random.randint(start_number, end_number) for j in range(num_columns)] for i in range(num_rows)]

    ## with the cycles
    # array = []
    # for i in range(num_rows):
    #     list_inside_matrix = []
    #     for j in range(num_columns):
    #         list_inside_matrix.append(random.randint(start_number, end_number))
    #     array.append(list_inside_matrix)
    # return array


# Task 6.2
def get_max_number(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(max(i))
    return max(list_max_numbers)


# Task 6.3
def get_min_number(array):
    list_min_numbers = []
    for i in array:
        list_min_numbers.append(min(i))
    return min(list_min_numbers)


# Task 6.4
def get_sum_all_numbers(array):
    summ = 0
    for i in array:
        for j in i:
            summ += j
    return summ


# Task 6.5
def get_index_row_max_sum_numbers(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(sum(i))
    return list_max_numbers.index(max(list_max_numbers))


# Task 6.6
def get_index_column_max_sum_numbers(array):
    list_max_numbers = []
    for i in range(len(array[0])):
        summ = 0
        for j in range(len(array)):
            summ += array[j][i]
        list_max_numbers.append(summ)
    return list_max_numbers.index(max(list_max_numbers))


# Task 6.7
def get_index_row_min_sum_numbers(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(sum(i))
    return list_max_numbers.index(min(list_max_numbers))


# Task 6.8
def get_index_column_min_sum_numbers(array):
    list_max_numbers = []
    for i in range(len(array[0])):
        summ = 0
        for j in range(len(array)):
            summ += array[j][i]
        list_max_numbers.append(summ)

    return list_max_numbers.index(min(list_max_numbers))


# Task 6.9
def set_zero_elements_above_main_diagonal(array):
    for i in range(len(array)):
        for j in range(i + 1, len(array[i])):
            array[i][j] = 0
    return array


# Task 6.10
def set_zero_elements_below_main_diagonal(array):
    for i in range(1, len(array)):
        for j in range(0, i):
            array[i][j] = 0
    return array


# Task 6.11
def create_two_matrix(m, n):
    matrix_a = create_matrix(-100000, 100000, m, n)
    matrix_b = create_matrix(-100000, 100000, m, n)

    return matrix_a, matrix_b


# Task 6.12
def get_summ_matrix(matrix_a, matrix_b):
    new_matrix = []
    for i in range(len(matrix_a)):
        new_list_for_matrix = []
        for j in range(len(matrix_a[i])):
            new_list_for_matrix.append(matrix_a[i][j] + matrix_b[i][j])
        new_matrix.append(new_list_for_matrix)

    return new_matrix


# Task 6.13
def get_sub_matrix(matrix_a, matrix_b):
    new_matrix = []
    for i in range(len(matrix_a)):
        new_list_for_matrix = []
        for j in range(len(matrix_a[i])):
            new_list_for_matrix.append(matrix_a[i][j] - matrix_b[i][j])
        new_matrix.append(new_list_for_matrix)
    return new_matrix


# Task 6.14
def get_multipl_g_matrix(g, matrix):
    new_matrix = []
    for i in range(len(matrix)):
        new_matrix_list = []
        for j in range(len(matrix[i])):
            new_matrix_list.append(matrix[i][j] * g)
        new_matrix.append(new_matrix_list)
    return new_matrix


# Task 7

def inch_to_cm(inch):
    return inch * 2.54


def cm_to_inch(cm):
    return cm / 2.54


def ml_to_km(mm):
    return mm * 1.609


def km_to_ml(km):
    return km / 1.609


def ft_to_kg(ft):
    return ft / 2.205


def kg_to_ft(kg):
    return kg * 2.205


def oz_to_g(oz):
    return oz * 28.3495


def g_to_oz(g):
    return g / 28.3495


def gallon_to_l(gallon):
    return gallon * 3.785


def l_to_gallon(l):
    return l / 3.785


def pint_to_l(pint):
    return pint / 2.113


def l_to_pint(l):
    return l * 2.113


# Task 8.1
def double_factorial(x):
    # double_factorial = 1
    # for i in range(x, 1, -2):
    #     double_factorial *= i
    # return double_factorial
    if x <= 0:
        return 1
    return x * double_factorial(x - 2)


# Task 8.2
def polindrom(word):
    return word == word[::-1]


def polindrom_in_polindroms(list_of_words):
    for i in list_of_words:
        if polindrom(i):
            return "There is a polydrome"
    return "There isn't a polydrome"


# Task 8.3
def sin1(x, eps):
    while (x > 2 * math.pi):
        x -= 2 * math.pi
    if eps <= 0:
        return "Use the epselon more thad 0"
    result_expression = x
    intermediate_result = x
    n = 1
    while abs(intermediate_result) > eps:
        intermediate_result = (((-1) ** n) * (x ** (2 * n + 1))) / (factorial(2 * n + 1))
        n += 1
        result_expression += intermediate_result
    return result_expression


def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)


def list_sin_with_different_precision(x, list_eps):
    return [sin1(x, eps) for eps in list_eps]


# Task 9.1
def format(list):
    return [f'{i + 1} - {v}' for i, v in enumerate(list)]


# Task 9.2
def doubling_arguments(**kwargs):
    return dict(map(lambda x: [x * 2, kwargs[x]], kwargs.keys()))


# Task 9.3
def decorator(fun):
    def wrapper(list_of_numbers):
        return fun([i for i in list_of_numbers if i % 2 != 0])

    return wrapper


@decorator
def numbers(list_numbers):
    return list_numbers


# Task 9.4
def decorator_reverse_args(fun):
    def wrapper(*args):
        if len(args) == 1:
            return args[0]
        new_args = args[::-1]
        return fun(*new_args)

    return wrapper


@decorator_reverse_args
def decorating_function(*args):
    return args
