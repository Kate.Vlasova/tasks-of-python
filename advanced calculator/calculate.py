from utilits import is_float, removing_unnecessary_brackets, prioritization, tuple_of_priority_operators


def calculation_expression(expression):
    # наконец приступим к вычислению выражения
    # для этого создадим цикл, который будет исследовать наше выражение до финального финала
    # пока не окажется, что внутри строки не осталось ничего кроме числа типа: float
    expression = removing_unnecessary_brackets(expression)
    while not is_float(expression):
        print(expression)
        # приравняем по умолчанию стартовую и окончательную позицию
        # первому и последнему элементу выражения, соответственно
        start = 0
        end = len(expression)

        # дальше проверим есть ли в выражении скобочки,
        # если есть - выставляем ограничение на выполнение выражения в скобочках
        if expression.find('(') != -1:
            start = expression.rfind('(')
            end = expression.find(')', start)

        # итак, ограничения выставлены, пройдёмся по кортежу приорететов,
        # т.е. постепенно перебирая элементы, будем находить операцию, которую нужно выполнить первой
        start_to_find = start
        for operator in prioritization():
            # в нулевом элементе может находиться минус, но именно этот минус нам неинтересен
            if start == 0 and expression[0] == '-':
                start_to_find = 1
            # если после скобки минус, то пропускаем его в поиске
            elif expression[start] == '(' and expression[start + 1] == '-':
                start_to_find = start + 2
            # ищем проверяемый оператор
            step = expression.find(operator, start_to_find, end)

            # если мы нашли минус, но левее его плюс, поищем ещё
            if step != -1 and step > 0:
                if expression[step] == '-' and expression[step - 1] == '+':
                    step = -1

            # если он найден
            if step != -1:
                # запускаем вычисление нашего очередного выражения
                expression = calculate_operation(expression, start, end, step, operator)
                # проверяем, не выскочила ли ошибка в процессе вычисления
                if expression.find('EXC:') != -1:
                    return expression
                # если выполнили вычисление начинаем всё заново
                break
        # пожалуй, могли появиться ненужные скобочки
        expression = removing_unnecessary_brackets(expression)
    return expression


def calculate_operation(expression, start, end, step, operator):
    # итак, нужно вычленить первый аргумент, для этого напишем небольшую функцию
    start = find_start_of_argument_1(expression, start, step)
    # старт нашли, теперь бы финиш
    end = find_end_of_argument_2(expression, end, step)
    # итак, аргументы найдены, пробуем привести их к числу с плавающей точкой,
    # преобразууем срез, тут внимательно, начинаем со старта, step не включаем
    argument_1 = float(expression[start:step])
    # преобразуем второй аргумент, step всё также игнорируем(там сидит оператор),
    # end включаем, странный питон, игнорирующий последний элемент
    argument_2 = float(expression[step + 1:end + 1])

    # вычисления, здесь все понятно
    if operator == '^':
        result = argument_1 ** argument_2
    elif operator == '*':
        result = argument_1 * argument_2
    elif operator == '/':
        # отловили ошибку, пересылаем её!
        try:
            result = argument_1 / argument_2
        except ZeroDivisionError:
            return 'EXC: Division Zero!!!'
    elif operator == '+':
        result = argument_1 + argument_2
    elif operator == '-':
        result = argument_1 - argument_2

    # # если нужно окгруглить
    # result = round(result, 3)

    # формируем новое выражение с учётом вычисленной части
    if end == len(expression):
        return expression[:start] + str(result)
    else:
        return expression[:start] + str(result) + expression[end + 1:]


def find_start_of_argument_1(expression, start, step):
    new_start = start
    # начнём с позиции левее оператора и будем двигаться влево пока не увидим, что число закончилось
    for i in range(step - 1, start - 1, -1):
        # если элемент число или точка, то всё в порядке
        if expression[i].isdigit() or expression[i] == '.':
            # присваиваем стартовую позицию
            new_start = i
            # и двигаемся дальше
            continue
        # если мы наткнулись на минус, тут уже могут быть варианты
        elif expression[i] == '-':
            # если это уже самое начало, значит слева ничего нет, Кэп
            if i == 0:
                # стартовая позиция
                new_start = 0
                # сваливаем
                break
            # если левее минуса расположился какой-либо из наших операторов либо '(', значит это отрицательное число
            elif expression[i - 1] in tuple_of_priority_operators or expression[i - 1] == '(':
                # захватим минус с собой
                new_start = i - 1
            # если же левее расположилось что-то другое, остовим его в покое
            else:
                # вернёмся на предыдуущую позицию
                new_start = i + 1
                # и леванем
                break
        else:
            # тут, собственно по старой схеме
            new_start = i + 1
            # bye, cycle
            break
    return new_start


def find_end_of_argument_2(expression, end, step):
    # если вдруг нам встретился минус, значит число отрицательное, включим его
    if expression[step + 1] == "-":
        step += 1
    # ищем конец второго аргумента
    for i in range(step + 1, end):
        # если это цифра или точка, значит это всё ещё число
        if expression[i].isdigit() or expression[i] == '.':
            # включаем его
            end = i
        else:
            # иначе это не число, расходимся
            break
    # нашли, отправляем
    return end
