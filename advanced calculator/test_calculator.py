from main import calculator


def test_calculator():
    exceptions = ' ' + "   ...1   -............5(1+1).7 - (-2)*******(2................)    + 5./(3-4)    ---(---3   * - +++++9  ///////  - -+ +-+++++- --18+(-(-(-(6+(7-5)))))  )-2**++----4*++7**********0    2+ (---6)((()((()))())-+-+-+--+-+-6) ++-(+--+-12-+-+5)++ ++-+ +9-+-+- 6 -4()(())-2,(3+2)"
    assert calculator(exceptions) == -773.1
    assert calculator('-1 - (-2) * 4') == 7
    assert calculator("7 + (3*******2  )  (-+4)-    4  ") == -33
    assert calculator('-6.83333-5359375.0+--3702.0-9-6-4') == -5355698.83333
    assert calculator('8 + -+-+-+-+-+-+++-++-+-+-+----+-+-+--+-++++5') == 3
    assert calculator('*1+3') == 'EXC: incorrect placement of operators in start'
    assert calculator('1+3**********') == 'EXC: Incorrect expression (exc in end)'
    assert calculator('1+-+-+-+-+*-+-+-+3') == 'EXC: incorrect placement of operators'
    assert calculator('54+5131+-32121* 5k-43') == 'EXC: Unrecognized item'
    assert calculator('(2+5)-5*(-5)+6)') == 'EXC: Incorrect quantity of bracket'
    assert calculator('(2+5)-5+(*1-(-5)+6)') == 'EXC: not that after the parenthesis'
    assert calculator('(2+5)-5+)((5*1)-(-5)+6') == 'EXS: Incorrect location of bracket'
    assert calculator('*') == 'EXC: Incorrect expression (exc in end)'
    assert calculator('.7') == 0.7
    assert calculator(-2 - -5) == 3
    assert calculator('5+7.8.9-1') == 'EXC: too many dots in one number'
    # добавим тестов для функций
    assert calculator('Sin(30)') == 0.5
    assert calculator('Sin(29,5 + coS(61.5 - 1,5))') == 1 / 2
    assert calculator('3+3+3-Tg') == "EXC: Incorrect expression (exc in end)"
    assert calculator('-2sIn30') == -1
    assert calculator("sin(30) / COS(30)") == calculator("tg(30)")
    assert calculator("TG(30)") == 1 / calculator("Ctg(30)")
    assert calculator('lg(10) ++++ ++ + + + + + lg(5-----5+++10***************2-1+-+-++--++1)') == 3
    assert calculator('ln(43)') == 3.7612001156935624
    assert calculator('log(56)') == 'EXC: Unrecognized item'
    assert calculator('s   i   n   (((cos(10*******2-40) +1-1)  *   2 +1)  ****4 - -  - -14)*2    0') == calculator(
        ' ((lg(100) ***** lg(10) - + -  -  -   - 1 + 1) ***-3 *1000---25) ****** 0    .    5')
    assert calculator('2 Sin(30)') == 1
    assert calculator('-sin-30.') == 0.5
    assert calculator('sin(cos60)') == 0.00872653549837
    assert calculator('sinCos60') == "EXC: incorrect function write"
    assert calculator('cos(sin30 - - +++ 59,5)') == 1 / 2
    # assert calculator() ==
