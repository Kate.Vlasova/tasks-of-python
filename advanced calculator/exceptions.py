from utilits import tuple_of_priority_operators, dict_of_functions


def find_exceptions(expression):
    output = []
    output.append(exception_quantity_brackets(expression))
    output.append(exception_correct_brackets(expression))
    output.append(exception_incorrect_element(expression))
    output.append(exception_incorrect_location_elements(expression))
    output.append(exeption_of_function(expression))

    for i in output:
        if i:
            return i


def exception_quantity_brackets(expression):
    # проверка корректного количества скобочек
    if expression.count('(') != expression.count(')'):
        return 'EXC: Incorrect quantity of bracket'


def exception_correct_brackets(expression):
    # проверим все ли скобочки расставлены правильно
    open = 0
    for i in expression:
        if i == '(':
            open += 1
        elif i == ')':
            open -= 1
        if open < 0:
            return 'EXS: Incorrect location of bracket'


def exception_incorrect_element(expression):
    # если появится какой-нибудь непонятный символ - ошибка
    for i in expression:
        if not (
                i.isdigit() or i in tuple_of_priority_operators or i == '(' or i == ')' or i == '.' or i in dict_of_functions):
            return 'EXC: Unrecognized item'


def exception_incorrect_location_elements(expression: str):
    # для проверки выражения поместим по краям служебные элементы,
    # это очень сильно облегчит логику, не позволяя выйти за пределы строки и получить тем самым ошибку
    expression = '|' + expression + '|'
    # попроверяем расположение нашей точки
    find_point = expression.find('.')
    while find_point != -1:
        # уточним, не находимся ли мы скраю (за пределы строки выйти не боимся благодаря первой строчке)
        if not (expression[find_point + 1].isdigit() or expression[find_point - 1].isdigit()):
            return 'EXC: incorrect location dot'
        # тут тоже был риск выйти за пределы стороки
        step = find_point + 1
        while step < len(expression):
            # если здесь цифра, то всё в порядке
            if expression[step].isdigit():
                step += 1
            # если здесь всплывёт точка, значит кидаем ошибку
            elif expression[step] == '.':
                return 'EXC: too many dots in one number'
            else:
                break
        find_point = expression.find('.', step)

    # проверим, чтобы перед закрывающей скобкой не было ничего кроме числа, точки и закрывающей скобочки
    find_bracket = expression.find(')')
    while find_bracket != -1:
        if expression[find_bracket - 1] != ')' and expression[find_bracket - 1] != '.' and not expression[
            find_bracket - 1].isdigit():
            return 'EXC: not that before the parenthesis'
        find_bracket = expression.find(')', find_bracket + 1)

    # проверим, чтобы после открывающей скобкой не было ничего кроме числа,
    # точки, минуса, функции и открывающей скобочки
    step = expression.find('(')
    while step != -1:
        element = expression[step + 1]
        if element != '(' and element != '.' and not element.isdigit() and element != '-' and element not in dict_of_functions:
            return 'EXC: not that after the parenthesis'
        step = expression.find('(', step + 1)

    # с точками разобрались, выносить в отдельную функцией это пока не буду
    # осталось проверить неправильное расположение операторов
    # создадим новый список без минуса, потому что он может идти за оператором, показывая отрицательное число
    list_operators_without_minus = list(tuple_of_priority_operators)
    list_operators_without_minus.remove('-')
    # пройдемся по выражению циклом и проверим
    for index, value in enumerate(expression):
        if value in tuple_of_priority_operators:
            # если рядом стоят два оператора - ошибка
            if expression[index + 1] in list_operators_without_minus:
                return 'EXC: incorrect placement of operators'

    # проверим нет ли чего вначале не того
    if expression[1] in list_operators_without_minus:
        return 'EXC: incorrect placement of operators in start'


def exeption_of_function(expression):
    # # если функция стоит в конце строки - значит что-то не то
    # if expression[-1] in dict_of_functions:
    #     return 'EXC: function in end'

    # проверяем, чтобы выражение после функции было в скобочках
    for i in range(len(expression)):
        if expression[i] in dict_of_functions:
            if expression[i + 1] != '(':
                return 'EXC: incorrect function write'
