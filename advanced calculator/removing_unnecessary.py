from trigonometric_functions import dict_of_functions


def removing_unnecessary_operators(expression: str):
    # пробелы нам точно не понадобятся, смело убираем
    expression = expression.replace(' ', '')
    # проведем дробные числа к единому формату
    expression = expression.replace(',', '.')
    # если есть лишние точки заменим их одной
    expression = processing_point(expression)
    # уберем пустые скобки
    expression = remove_the_empty_brackets(expression)
    # также нужно проверить, нет ли в конце выражения чего-нибудь лишнего
    if not (expression[-1].isdigit() or expression[-1] == '.' or expression[-1] == ')' or expression[-1] == ','):
        return 'EXC: Incorrect expression (exc in end)'
    # понеслось
    # начнём с минусов
    expression = processing_minus(expression)
    # плюсы
    expression = processing_plus(expression)
    # пришло время лишних делений
    expression = processing_divide(expression)
    # наконец уберем лишние умножени и выясним, будет ли у нас возведение в степень
    expression = processing_multiply(expression)
    # поработаем над форматом входящих функций
    expression = make_correct_functions(expression)
    # Вывод
    return expression


# если подряд несколько точек, меняем на одну
def processing_point(expression: str):
    # пока есть что менять - меняем
    while expression.find('..') != -1:
        expression = expression.replace('..', '.')
    return expression


def processing_minus(expression):
    # removing unnecessary '-' (Убираем лишние минусы)

    # находим минус
    step = expression.find('-')
    # цикл работает пока находит необработанные минусы
    while step != -1:
        # считаем минусы
        counter_minus = 0
        # идём по строке пока в ней присутствуют минусы и плюсы подряд
        while expression[step] == '-' or expression[step] == '+':
            # если находим минус удаляем его и увеличиваем счётчик минусов
            if expression[step] == '-':
                expression = expression[:step] + expression[step + 1:]
                counter_minus += 1
                # тут нужно учитывать, что при удаленни элемента новый попадает на его место
                step -= 1
            step += 1
        # если количество минусов нечетное, то вставляем минус в проверяемое место,
        # иначе поставим плюс (все минусы нейтрализованы)
        if counter_minus % 2 != 0:
            expression = expression[:step] + '-' + expression[step:]
        else:
            expression = expression[:step] + '+' + expression[step:]
        step += 1
        # переходим к следующему элементу
        step += 1
        # ищем новые минусы
        step = expression.find('-', step)
    return expression


def processing_plus(expression):
    # removing unnecessary '+' (убираем лишние плюсы)

    # находим плюс
    step = expression.find('+')
    # пока есть необработанные плюсы выполняем
    while step != -1:
        # пока плюсы идут подряд убираем их
        while expression[step] == '+' and (
                expression[step + 1] == '+' or expression[step + 1] == '-'):
            expression = expression[:step] + expression[step + 1:]
        # переходим к следующему элементу
        step += 1
        # ищем новые плюсы
        step = expression.find('+', step)
    return expression


def processing_divide(expression):
    # removing unnecessary '/' (удаляем лишние деления)
    # находим оператор деления
    step = expression.find('/')
    # выполняем пока есть новые операторы деления
    while step != -1:
        # пока операторы идут подряд - удаляем лишние
        while expression[step] == '/' and expression[step + 1] == '/':
            expression = expression[:step] + expression[step + 1:]
        step += 1
        # если по пути найдём какой-нибудь лишний ненужный плюс, пожалуй, тоже его удалим
        if expression[step] == '+':
            expression = expression[:step] + expression[step + 1:]
            step -= 1
        # ну и, собственно, ищем следующее деление
        step = expression.find('/', step)
    return expression
    # взятие квадратного корня будет реализовано в следующей версии программы


def processing_multiply(expression):
    # removing unnecessary '*' (удалим лишние умножения)
    # находим первое умножение
    step = expression.find('*')
    # пока есть необработанные умножения - выполняем
    while step != -1:
        # тут установим переменную для отлова возведения в степень
        multiply = False
        # пока находим подряд умножение - продолжаем
        while expression[step] == '*' and expression[step + 1] == '*':
            expression = expression[:step] + expression[step + 1:]
            # если было несколько умножений значит нужно возводить в степень
            multiply = True
        # наконец, обозначим умножение, введением своего оператора
        if multiply:
            expression = expression[:step] + '^' + expression[step + 1:]
        step += 1
        # вдруг встретится какой-то лишний плюс нужно его подчистить
        if expression[step] == '+':
            expression = expression[:step] + expression[step + 1:]
            step -= 1
        # находим следующее умножение
        step = expression.find('*', step)
    return expression


def remove_the_empty_brackets(expression):
    # remove the empty brackets (Убираем пустые скобки)
    # находим скобочку
    step = expression.find('(')
    # выполняем пока есть новые скобочки
    while step != -1:
        # если скобочки оказались пустыми, пожалуй, удалим их
        if expression[step + 1] == ')':
            expression = expression[:step] + expression[step + 2:]
            # при удалении скобочек откатимся назад, чтобы не пропустить ещё больше скобочек
            step -= 2
        # берем новый элемент
        step += 1
        # ищем новую скобочку
        step = expression.find('(', step)
    return expression


def make_correct_functions(expression):
    # сделаем все буквы прописными, вдруг пользователь что-то напутал
    expression = expression.upper()
    # Заменим на условные обозначения
    for design, func in dict_of_functions.items():
        expression = expression.replace(func, design)

    step = 0
    while step < len(expression):
        # если перед функцией стоит число, нужно поставить между ними умножение:
        if expression[step] in dict_of_functions:
            if step == 0:
                pass
            elif expression[step - 1].isdigit() or expression[step - 1] == '.':
                expression = expression[:step] + '*' + expression[step:]
        # у
        step += 1

    # вставим сюда небольшой кодик который делает следующее:
    # если после функции идёт число без круглых скобок, возьмём его в скобки
    step = 0
    while step < len(expression) - 1:
        if expression[step] in dict_of_functions:
            if expression[step + 1].isdigit() or expression[step + 1] == '-' or expression[step + 1] == '.' or \
                    expression[step + 1] in dict_of_functions:
                expression = expression[:step + 1] + '(' + expression[step + 1:]
                step += 2
                while expression[step].isdigit() or expression[step] == '-' or expression[step] == '.' or expression[
                    step] in dict_of_functions:
                    if step == len(expression) - 1:
                        expression = expression + ')'
                        step += 2
                        break
                    step += 1
                if step != len(expression):
                    expression = expression[:step] + ')' + expression[step:]
        step += 1

    return expression
