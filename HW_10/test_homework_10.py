from homework_10 import *
from datetime import datetime, date, timedelta
import csv


def test_grouping_people():
    # information for create
    fields = ["Firstname", "Lastname", "Age"]
    rows = [
        ["Kate", "Vlasova", 25],
        ["Kirill", "Kulesh", 29],
        ["Valentin", "Kutusov", 56],
        ["Angelina", "Denishchik", 23],
        ["Victoria", "Denishchik", 7],
        ["Dmitry", "Denishchik", 1],
        ["Galina", "Vlasova", 57],
        ["Ludmila", "Petrova", 55],
        ["Aleksandr", "Petrow", 30],
        ["Aleksandra", "Pechkina", 18],
        ["Jack", "Divulskiy", 87]
    ]

    fileName = "people_info.csv"
    fileNameReport = 'report_of_group_people.csv'

    # start testing program
    grouping_people(fields, rows, fileName, fileNameReport)

    # Create results
    list_of_group_people = [['interval', 'count'], ['1-12', 2], ['13-18', 1], ['19-25', 2], ['26-40', 2], ['40+', 4]]
    report_group_people = []
    reading_people_info = []

    # reading created file 'people_info'
    with open(fileName, 'r') as people_info:
        peopls = csv.reader(people_info)
        for people in peopls:
            if people[2].isdigit():
                people[2] = int(people[2])
            reading_people_info.append(people)

    # reading testing file
    with open(fileNameReport, 'r') as report_file:
        report = csv.reader(report_file)
        for row in report:
            if row[1].isdigit():
                row[1] = int(row[1])
            report_group_people.append(row)

    # test
    assert [fields, *rows] == reading_people_info
    assert list_of_group_people == report_group_people


def test_average_weather():
    fields = ["Date", "Place", "Degrees", "Wind speed"]
    rows = [
        [date(2021, 3, 28), "Minsk", 7, 5],
        [date(2021, 3, 28), "Gomel", 9, 3],
        [date(2021, 3, 29), "Minsk", 8, 5],
        [date(2021, 3, 29), "Gomel", 10, 3],
        [date(2021, 3, 30), "Minsk", 7, 8],
        [date(2021, 3, 30), "Gomel", 9, 3],
        [date(2021, 3, 31), "Minsk", 12, 6],
        [date(2021, 3, 31), "Gomel", 14, 6],
        [date(2021, 4, 1), "Minsk", 11, 3],
        [date(2021, 4, 1), "Gomel", 12, 3],
        [date(2021, 4, 2), "Minsk", 17, 4],
        [date(2021, 4, 2), "Gomel", 18, 8],
        [date(2021, 4, 3), "Minsk", 11, 5],
        [date(2021, 4, 3), "Gomel", 11, 3],
        [date(2021, 4, 4), "Minsk", 12, 8],
        [date(2021, 4, 4), "Gomel", 14, 5],
        [date(2021, 4, 5), "Minsk", 9, 5],
        [date(2021, 4, 5), "Gomel", 10, 3],
        [date(2021, 4, 6), "Minsk", 14, 5],
        [date(2021, 4, 6), "Gomel", 16, 3],
        [date(2021, 4, 7), "Minsk", 17, 5],
        [date(2021, 4, 7), "Gomel", 18, 3]
    ]

    today = date(2021, 4, 7)
    file_name = file_creator(fields, rows)
    average_weather(file_name, today) == (27.142857142857142, 9)


def test_find_earliest_date():
    # create a file with dates
    file_name = 'date_inf.csv'
    list_of_dates = [[date(2021, 10, 4), date(2021, 11, 5), date(2022, 6, 15), date(2013, 7, 18)],
                     [date(1965, 4, 4), date(2048, 6, 24), date(2000, 3, 16), date(2011, 5, 5), date(1028, 10, 28)],
                     [date(2525, 11, 22), date(1654, 12, 21), date(1995, 8, 17)]]
    with open(file_name, 'w') as date_file:
        writer_date = csv.writer(date_file)
        writer_date.writerows(list_of_dates)

    # test
    assert find_earliest_date(file_name) == date(1028, 10, 28)
